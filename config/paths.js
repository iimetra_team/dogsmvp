const path = require('path');

const appDirectory = path.resolve(__dirname, '..');

const resolvePath = (relativePath) => path.resolve(appDirectory, relativePath);

module.exports = {
  buildPath: resolvePath('build'),
  uiHtml: resolvePath('src/index.html'),
  uiHtmlOutputName: 'index.html',
  uiIndex: resolvePath('src/index.js'),
  uiSrc: resolvePath('src'),
};
