const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

const paths = require('../paths');
const PACKAGE = require('../../package.json');

module.exports = {
  devServer: {
    compress: true,
    contentBase: paths.buildPath,
    disableHostCheck: true,
    historyApiFallback: true,
    hot: true,
    port: 8086
  },
  devtool: 'cheap-module-source-map',
  entry: {
    main: paths.uiIndex
  },
  mode: 'development',
  module: {
    rules: [
      {
        exclude: /node_modules/,
        test: /\.js$/,
        use: {
          loader: 'babel-loader'
        },
      }, {
        include: [
          paths.uiSrc
        ],
        loader: 'babel-loader',
        test: /\.jsx?$/
      }, {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }, {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      }, {
        loader: 'file-loader',
        test: /\.(eot|ttf|svg|gif|png|jpg|ico)$/i
      }
    ]
  },
  output: {
    filename: '[name].js',
    path: paths.buildPath,
    publicPath: '/'
  },
  plugins: [
    new HtmlWebpackPlugin({
      chunks: ['main'],
      favicon: './src/favicon.ico',
      filename: paths.uiHtmlOutputName,
      inject: true,
      template: paths.uiHtml
    }),
    new webpack.BannerPlugin(`${PACKAGE.name}: version ${PACKAGE.version}`),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.ModuleConcatenationPlugin()
  ],
  resolve: {
    extensions: ['.mjs', '.js', '.jsx']
  }
};
