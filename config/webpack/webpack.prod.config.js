const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const PACKAGE = require('../../package.json');
const paths = require('../paths');

module.exports = {
  entry: paths.uiIndex,
  mode: 'production',
  module: {
    rules: [{
      include: [
        paths.uiSrc
      ],
      loader: 'babel-loader',
      test: /\.jsx?$/
    }, {
      loader: 'style-loader!css-loader',
      test: /\.css$/
    }, {
      loaders: ['style-loader', 'css-loader', 'sass-loader'],
      test: /\.scss$/
    }, {
      loader: 'file-loader',
      test: /\.(eot|ttf|svg|gif|png|jpg|ico)$/i
    }]
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        parallel: true
      })
    ]
  },
  output: {
    path: paths.buildPath,
    publicPath: '/'
  },
  plugins: [
    new HtmlWebpackPlugin({
      favicon: './src/favicon.ico',
      template: paths.uiHtml
    }),
    new CompressionPlugin(),
    new webpack.BannerPlugin(`${PACKAGE.name}: version ${PACKAGE.version}`),
    new webpack.optimize.ModuleConcatenationPlugin(),
  ],
  resolve: {
    extensions: ['.mjs', '.js', '.jsx']
  }
};
