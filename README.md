# DogsMVP

App host: https://dog-library.iimetra.xyz/

Public Free API: https://docs.thedogapi.com/

`npm start` - to run locally
`npm run test` - to run tests
`npm run lint` - to run linter
`npm run build` - to run build

`buildspec.yaml` is used by CodeBuild to build and deploy application to S3

AWS Services used:
S3, IAM, CloudFront, KMS, CodeBuild, Route53