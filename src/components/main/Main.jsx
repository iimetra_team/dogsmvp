import React from 'react';
import Router from '../router';

const Main = () => {
  return (
    <Router />
  );
};

export default Main;
