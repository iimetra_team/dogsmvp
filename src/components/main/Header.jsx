import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(() => ({
  title: {
    flexGrow: 1,
  },
}));

const Header = () => {
  const classes = useStyles();

  return (
    <AppBar position="static">
      <Toolbar>
        <Button className={classes.title} color="inherit" component={Link} to={'/'}>
          <Typography className={classes.title} variant="h6">
              Dog Library
          </Typography>
        </Button>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
