import React from 'react';
import { Route, Switch } from 'react-router-dom';
import DogGrid from './view/DogGrid';
import DogView from './view/DogView';

const Router = () => {
  return (
    <Switch>
      <Route component={DogGrid} exact={true} path="/" />
      <Route path="/dogs/:id" render={(props) => <DogView id={props.match.params.id} />} />
    </Switch>
  );
};

export default Router;
