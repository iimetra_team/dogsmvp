import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { getDogBy, getDogImageBy } from '../../services/dog-service';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles(() => ({
  card: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
  },
  cardContent: {
    flexGrow: 1,
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
}));

const DogView = (props) => {
  const classes = useStyles();
  const { id } = props;

  const [dog, setDog] = useState(null);
  const [imageUrl, setImageUrl] = useState(null);

  useEffect(() => {
    getDogBy(id).then((dogResp) => setDog(dogResp));
    getDogImageBy(id).then((url) => setImageUrl(url));
  }, []);

  return (
    <div>
      {dog && imageUrl && (
        <Card className={classes.card}>
          <CardMedia
            className={classes.cardMedia}
            image={imageUrl}
            title="Random Image"
            />
          <CardContent className={classes.cardContent}>
            <Typography component="h2" gutterBottom={true} variant="h5">
              {dog.name}
            </Typography>
            <Typography>
              Origin: {dog.origin || 'Unknown'}
              <br />
              Temperament: {dog.temperament}
            </Typography>
            <Typography>
              Bred for: {dog.bred_for}
            </Typography>
            <Typography>
              Breed group: {dog.breed_group}
            </Typography>
            <Typography>
              Weight: {dog.weight.metric}
            </Typography>
            <Typography>
              Height: {dog.height.metric}
            </Typography>
            <Typography>
              Life span: {dog.life_span}
            </Typography>
          </CardContent>
        </Card>
      )}
    </div>
  );
};

DogView.propTypes = {
  id: PropTypes.string,
};

export default DogView;
