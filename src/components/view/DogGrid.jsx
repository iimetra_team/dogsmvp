import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Skeleton from '@material-ui/lab/Skeleton';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import { useHistory } from 'react-router-dom';
import makeStyles from '@material-ui/core/styles/makeStyles';
import {
  getDogImageBy,
  receiveDogList
} from '../../services/dog-service';

const useStyles = makeStyles((theme) => ({
  card: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
  },
  cardContent: {
    flexGrow: 1,
  },
  cardGrid: {
    paddingBottom: theme.spacing(8),
    paddingTop: theme.spacing(8),
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  skeleton: {
    variant: 'rect'
  },
}));

const DogGrid = () => {
  const classes = useStyles();
  const appHistory = useHistory();

  const [loading, setLoading] = useState(true);
  const [randomImageUrl, setRandomImageUrl] = useState(null);
  const [dogList, setDogList] = useState([]);

  useEffect(() => {
    receiveDogList()
    .then((response) => setDogList(response))
    .then(() => setLoading(false));
    getDogImageBy()
    .then((url) => setRandomImageUrl(url));
  }, []);

  const viewDogInformation = (e, id) => {
    e.preventDefault();
    appHistory.push(`/dogs/${id}`);
  };

  return (
    <div>
      <Container className={classes.cardGrid} maxWidth="md">
        <Grid container={true} spacing={4}>
          {(loading ? Array.from(new Array(15)) : dogList).map((dog, index) => (
            <Grid item={true} key={index} md={4} sm={6} xs={12}>
              {dog ? (
                <Card className={classes.card}>
                  <CardMedia
                    className={classes.cardMedia}
                    image={randomImageUrl}
                    title="Random Image"
                    />
                  <CardContent className={classes.cardContent}>
                    <Typography component="h2" gutterBottom={true} variant="h5">
                      {dog.name}
                    </Typography>
                    <Typography>
                            Origin: {dog.origin || 'Unknown'}
                      <br />
                            Temperament: {dog.temperament}
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button color="primary" onClick={(e) => viewDogInformation(e, dog.id)} size="small">
                            View Full Information
                    </Button>
                    <Button color="primary" disabled={true} size="small">
                            Edit
                    </Button>
                  </CardActions>
                </Card>
              ) : (
                <Skeleton
                  className={classes.skeleton}
                  height={118}
                  key={index}
                  width={210}
                  />
              )}
            </Grid>
          ))}
        </Grid>
      </Container>
    </div>
  );
};

export default DogGrid;
