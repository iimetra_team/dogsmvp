import React from 'react';
import './app.scss';
import Header from './components/main/Header';
import Main from './components/main/Main';

const App = () => {
  return (
    <div>
      <Header />
      <Main />
    </div>
  );
};

export default App;
