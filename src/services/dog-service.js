const API_KEY = '416bd6e6-717d-4aea-b8e1-967aa2ddd075';
const DOG_DESCRIPTION_ENDPOINT = 'https://api.thedogapi.com/v1';

export async function receiveDogList() {
  return await fetch(`${DOG_DESCRIPTION_ENDPOINT}/breeds`, {
    headers: {
      'x-api-key': API_KEY
    }
  })
    .then((res) => res.json())
    .catch((err) => new Error(err));
}

export async function getDogBy(id) {
  return await fetch(`${DOG_DESCRIPTION_ENDPOINT}/breeds/${id}`, {
    headers: {
      'x-api-key': API_KEY
    }
  })
    .then((res) => res.json())
    .catch((err) => new Error(err));
}

export async function getDogImageBy(dogId) {
  return await fetch(`${DOG_DESCRIPTION_ENDPOINT}/images/search?breed_id=${dogId ? dogId : getRandomImage()}`, {
    headers: {
      'x-api-key': API_KEY
    }
  })
    .then((res) => res.json())
    .then((json) => json[0].url)
    .catch((err) => new Error(err));
}

function getRandomImage() {
  return Math.floor(Math.random() * (100 - 1)) + 1;
}

export async function findADogByQuery(query) {
  return await fetch(`${DOG_DESCRIPTION_ENDPOINT}/breeds/search?q=${query}`)
    .then((res) => res.json())
    .catch((err) => new Error(err));
}
